<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Delegacion */

$this->title = 'Create Delegacion';
$this->params['breadcrumbs'][] = ['label' => 'Delegacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delegacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
