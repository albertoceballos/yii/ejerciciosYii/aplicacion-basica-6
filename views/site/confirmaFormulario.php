<?php
use yii\helpers\Html;
?>
<p>Has introducido la siguiente información</p>

<ul>
    <li><label>Nombre</label>: <?= Html::encode($model->name) ?></li>
    <li><label>Edad</label>: <?= Html::encode($model->edad) ?></li>
    <li><label>Peso</label>: <?= Html::encode($model->peso) ?></li>
    <li><label>Altura</label>: <?= Html::encode($model->altura) ?></li>
</ul>

