<?php

namespace app\models;

use yii\base\Model;

class Formulario extends Model
{
    public $name;
    public $edad;
    
    public function rules()
    {
        return [
            [['name', 'edad'], 'required','message'=>'{attribute} es obligatorio'],
            [['name', 'edad'], 'safe'],
            ['edad', 'integer', 'min' => 0,'max'=>'100','tooBig'=>'la edad máxima es 100','tooSmall'=>'la edad mínima es 0'],
        ];
    }
    
    public function attributeLabels(){
        return ['name'=>'Nombre del trabajador',
            'edad'=>'Edad del trabajador'];
    }
}