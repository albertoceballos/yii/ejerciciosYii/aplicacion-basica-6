<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

?>
<p>Información del trabajador</p>

<ul>
    <li><label>Nombre</label>:<?= Html::encode($model->nombre) ?></li>
    <li><label>Apellidos</label>:<?= Html::encode($model->apellidos) ?></li>
    <li><label>Peso</label>:<?= Html::encode($model->peso) ?></li>
    <li><label>Altura</label>:<?= Html::encode($model->altura) ?></li>
    <li><label>Poblacion</label>:<?= Html::encode($model->poblacion) ?></li>
    <li><label>Nombre Completo</label>:<?= Html::encode($model->getNombrecompleto()) ?></li>
    <li><label>Índice de Masa Corporal</label>:<?= Html::encode($model->getImc()) ?></li>
</ul>


<div>Mediante Detailviews:</div>
<?= DetailView::widget(
        [
            'model'=>$model,
            'attributes'=>[ 
                [            
                   'label' => 'Nombre: ',
                    'value' => "$model->nombre",
                ],
                    
            ],
            'options'=>[
                'class'=>'col-md-3 table-striped table-bordered',
            ],          
        ]
    );
 ?>
<?= DetailView::widget(
        [
            'model'=>$model,
            'attributes'=>[ 
                [            
                   'label' => 'Apellidos: ',
                    'value' => "$model->apellidos",
                ],
                    
            ],
            'options'=>[
                'class'=>'col-md-3 table-striped table-bordered',
            ],          
        ]
    );
 ?>