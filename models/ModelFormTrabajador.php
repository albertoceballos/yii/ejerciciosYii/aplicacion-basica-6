<?php

namespace app\models;

use yii\base\Model;

class ModelFormTrabajador extends Model
{
    public $nombre;
    public $apellidos;
    public $peso;
    public $altura;
    public $items;
    public $poblaciones=["Santander"=>"Santander","Laredo"=>"Laredo","Potes"=>"Potes"];
    public $poblacion;
    public $nombrecompleto;
    public $imc;
    
    
    public function rules()
    {
        return [
            [['nombre', 'peso','altura'],'required','message'=>'{attribute} es obligatorio'],
            ['nombre','string','max'=>20,'tooLong'=>'máximo de 20 caracteres'],
            ['apellidos','string', 'max'=>50,'tooLong'=>'máximo de 50 caracteres'],
            [['nombre', 'peso','altura','poblacion'], 'safe'],
            ['peso','integer'],
            ['altura','integer', 'min' => 100,'max'=>250,'tooBig'=>'la altura máxima es 250','tooSmall'=>'la altura mínima es 100'],
        ];
    }
    
    public function attributeLabels(){
        return ['name'=>'Nombre del trabajador',
            'edad'=>'Edad del trabajador',
            'peso'=>'Peso del trabajador',
            'poblaciones'=>'Selecciona población',
            'altura'=>'Altura del trabajador'];
            
    }
    
    public function getNombrecompleto(){
        $this->nombrecompleto= $this->nombre." ".$this->apellidos;
        return $this->nombrecompleto;
    }
    
    public function getImc(){
        $this->imc= $this->peso/( pow($this->altura,2));
        return $this->imc;
    }
}

