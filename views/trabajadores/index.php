<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trabajadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellidos',
            'fechaNacimiento',
            ['attribute' => 'foto',
              'format' => 'html',    
              'value' => function ($data) {
                return Html::img(Yii::getAlias('@web').'/imgs/'. $data['foto'],
                    ['width' => '140px']);
                },
            ],
            'delegacion',
            //'delegacion0.nombre',
            ['attribute'=>'poblacion',
             'value'=>'delegacion0.poblacion',
                ],
            ['attribute'=>'nombredelegacion',
             'value'=>'delegacion0.nombre',
                ],
            
            

            ['class' => 'yii\grid\ActionColumn',
             'template' =>'{view}{update}{delete}{delegacion}',
             'buttons'=>['delegacion'=>function($url,$model){
                            return Html::a("<i class='glyphicon glyphicon-new-window'></i>",["delegacion/view","id"=>$model->delegacion]);
                        }
             ]
            ],
        ],
    ]); ?>
</div>
