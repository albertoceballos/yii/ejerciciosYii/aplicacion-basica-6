<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//var_dump($model->poblaciones);
?>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre') ?>
    <?= $form->field($model, 'apellidos') ?>
    <?= $form->field($model, 'peso') ?>
    <?= $form->field($model, 'altura') ?>
    <?= $form->field($model, 'poblacion')->dropDownList(
                $model->poblaciones );
    ?>

    <div class="form-group">
        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

