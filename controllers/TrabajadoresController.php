<?php

namespace app\controllers;

use Yii;
use app\models\Trabajadores;
use app\models\TrabajadoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ModelFormTrabajador;

/**
 * TrabajadoresController implements the CRUD actions for Trabajadores model.
 */
class TrabajadoresController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Trabajadores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrabajadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Trabajadores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Trabajadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Trabajadores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Trabajadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Trabajadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Trabajadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Trabajadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Trabajadores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
    public function actionFormtrabajador(){
        $model=new ModelFormTrabajador;
     
          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
              
              return $this->render('datosTrabajador',['model'=>$model]);
          }else{
              return $this->render('formTrabajador',['model'=>$model]);
          }
    }


    /* Realizar un formulario que permita introducir:
 * 
 * -Nombre(obligatorio y maximo 20)
 * -Apellidos(max 50)
 * -Poblacion (desplegable: Santander,Potes, Laredo)
 * -Peso (obligatorio, numero)
 * -altura(obligatorio,numero, entre 100 y 250 Cm)
 * 
 * Cuando el usuario envie el formulario se muestre en la vista:
 * -Nombre Completo(la concatenacion debe realizarla el modelo getNombrecompleto)
 * -Mostrar el IMC (peso/altura) debe realizarlo el modelo getImc;
 */
    
    
}
